import requests

uri = "https://httpbin.org/ip"

response = requests.get(uri)
output = open("output.txt","w")
output.write("\n Response Body: \n" + str(response.text))
output.write("\n Content-Type:" + str(response.headers['Content-Type']))
output.write("\n Status Code:" + str(response.status_code))
output.write("\n Response Encoding:" + str(response.encoding))
